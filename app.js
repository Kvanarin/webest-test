document.addEventListener("DOMContentLoaded", function () {
  const listProducts = [
    {
      id: 0,
      name: "Товар №1",
      description:
        "Описание товара №1 гласит, что данный товар описан в полном объеме.",
      image: "images/product-image1.png",
      remains: 6,
      selected: 0,
    },
    {
      id: 1,
      name: "Товар №2",
      description:
        "Описание товара №2 гласит, что даный товар описан в полном объёме.",
      image: "images/product-image2.png",
      remains: 5,
      selected: 0,
    },
    {
      id: 2,
      name: "Товар №3",
      description:
        "Описание товара №3 гласит, что даный товар описан в полном объёме.",
      image: "images/product-image3.png",
      remains: 4,
      selected: 0,
    },
    {
      id: 3,
      name: "Товар №4",
      description:
        "Описание товара №4 гласит, что даный товар описан в полном объёме.",
      image: "images/product-image4.png",
      remains: 3,
      selected: 0,
    },
    {
      id: 4,
      name: "Товар №5",
      description:
        "Описание товара №5 гласит, что даный товар описан в полном объёме.",
      image: "images/product-image5.png",
      remains: 2,
      selected: 0,
    },
    {
      id: 5,
      name: "Товар №6",
      description:
        "Описание товара №6 гласит, что даный товар описан в полном объёме.",
      image: "images/product-image6.png",
      remains: 1,
      selected: 0,
    },
  ];

  let basketProducts = [];
  const modalDetails = document.querySelector("#modal-details");
  const modalContainer = document.querySelector("#modal-container");
  const basketCount = document.querySelector("#basket-count");
  const localBasketProducts = localStorage.getItem("basketProducts");

  // Функция подсчета количества добавленных в корзину товаров
  const calculateProductsBasket = (basketProducts) => {
    const numberBasket = basketProducts.reduce(
      (accumulator, currentValue) => accumulator + currentValue.selected,
      0
    );
    basketCount.textContent = numberBasket;
  };

  // Функция закрытия модального окна
  const closeModal = () => {
    modalDetails.classList.add("modal-none");
  };

  if (localBasketProducts) {
    basketProducts = JSON.parse(localBasketProducts);

    calculateProductsBasket(basketProducts);

    listProducts.forEach((product) => {
      const foundProduct = basketProducts.find(
        (item) => item.id === product.id
      );
      if (foundProduct) {
        product.remains = foundProduct.remains;
        product.selected = foundProduct.selected;
      }
    });
  }

  // Отрисовка карточек товара
  const cardsList = document.querySelector(".cards-list");
  listProducts.forEach((product) => {
    const card = document.createElement("li");
    card.classList.add("product-card");

    card.innerHTML = `
    <h2 class="product-card__name">${product.name}</h2>
    <img class="product-card__img" src="${product.image}" alt="${product.name}" />
    <p class="product-card__description">Описание товара: ${product.description}</p>
    <button id="${product.id}" class="product-card__button">Подробнее</button>
  `;

    cardsList.appendChild(card);
  });

  const getProductById = (productId) =>
    listProducts.find((product) => product.id === parseInt(productId));

  // При клике на "Подробнее" в карточке товара
  cardsList.addEventListener("click", (event) => {
    if (event.target.classList.contains("product-card__button")) {
      const productId = event.target.id;
      const selectedProduct = getProductById(productId);

      modalDetails.classList.remove("modal-none");
      modalContainer.innerHTML = `
      <div id="modal-close" class="modal-details__close"></div>
      <h3 class="modal-details__name">${selectedProduct.name}</h3>
      <p class="modal-details__details">${selectedProduct.description}</p>
      <div class="modal-details__remains">
        <p>Остаток: <span>${selectedProduct.remains}</span> шт</p>
        <button id="addProduct" class="modal-details__button">Добавить в корзину</button>
      </div>
    `;

      if (selectedProduct.remains === 0) {
        const disAddProduct = document.querySelector("#addProduct");
        disAddProduct.classList.add("disable");
      }

      const closeBtn = document.getElementById("modal-close");
      closeBtn.addEventListener("click", closeModal);

      // Функция добавления в корзину
      const addProductBtn = document.getElementById("addProduct");
      addProductBtn.addEventListener("click", () => {
        const productInBasket = basketProducts.find(
          (item) => item.id === selectedProduct.id
        );

        if (selectedProduct.remains > 0) {
          selectedProduct.remains--;
          selectedProduct.selected++;

          if (productInBasket) {
            productInBasket.remains--;
            productInBasket.selected++;
          } else {
            basketProducts.push({ ...selectedProduct });
          }

          localStorage.setItem(
            "basketProducts",
            JSON.stringify(basketProducts)
          );
          calculateProductsBasket(basketProducts);
          closeModal();
        }
      });
    }
  });

  // Закрытие модального окна по клику вне его
  modalDetails.addEventListener("click", (event) => {
    if (!modalContainer.contains(event.target)) {
      closeModal();
    }
  });
});
